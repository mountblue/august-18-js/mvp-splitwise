import React, { Component } from 'react';
import { BrowserRouter as Router,Switch } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Login from './Login';
import Signup from './Signup';
import Header from '../Header';

class Splitwise extends Component {
  render() {
    return (
      <Router>
        <div>
          <Header/>
          <Switch>
          <Route path="/" exact render={(props)=> <Login {...props} />} />
          <Route path="/Signup/" exact render={(props)=> <Signup {...props} />} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Splitwise;
