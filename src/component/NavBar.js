import React, { Component } from 'react';

export default class Splitwise extends Component {
  constructor() {
    super();
    this.displayDashboard = this.displayDashboard.bind();
    this.displayExpenses = this.displayExpenses.bind();
    this.addFriend = this.addFriend.bind();
  }

  displayDashboard() {}

  displayExpenses() {}

  addFriend() {}

  render() {
    return (
      <div className="side-bar">
        <nav>
          <button className="sidebar-button" onClick={this.displayDashboard} type="button">Dashboard</button>
          <br />
          <button className="sidebar-button" onClick={this.displayExpenses} type="button">All expenses</button>
          <br />
          <span className="sidebar-button add-friend">
            Friends
            <button type="button" onClick={this.addFriend} className="add-friend-button">+add</button>
          </span>
          <button className="sidebar-button" type="button">Friend 1</button>
          <br />
          <button className="sidebar-button" type="button">Friend 2</button>
        </nav>
      </div>
    );
  }
}
