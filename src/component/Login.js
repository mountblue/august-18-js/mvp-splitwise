import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { logIn } from "../action/Action";
import { Button,FormControl } from "react-bootstrap";
import "../styles/login.css";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      userEmail: "",
      userPassword: ""
    };
    this.onClickLogin = this.onClickLogin.bind(this);
    this.onClickSignup = this.onClickSignup.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onClickLogin(e) {
    e.preventDefault();
    this.props.logIn(this.state);
    this.props.history.push("/");
  }

  onClickSignup(e) {
    e.preventDefault();
    this.props.history.push("/Signup/");
  }

  render() {
    let errorMessage;
    switch (this.props.loginStatus.authError) {
      case "auth/invalid-email":
        errorMessage = "user email is invalid....";
        break;
      case "auth/wrong-password":
        errorMessage = "user password is invalid....";
        break;
      default:
      errorMessage='';
      break;
    }
    return (
      <div className="Login">
        <div className="header">
          <img src="http://plates.splitwise.com/splitwise-logo-bordered.png" />
          <h1>Split expenses with friends.</h1>
        </div>
        <div className="showcase">
          <img src="https://www.msfrugalears.com/wp-content/uploads/2018/05/splitwise-850x550.png" />
          <form>
            <h6>Email:</h6>
            <FormControl
              id="FormControlsEmail"
              type="email"
              name="userEmail"
              placeholder="Enter Email"
              onChange={this.onChange}
            />
            <br />
            <h6>Password:</h6>
            <FormControl
              id="FormControlsPassword"
              type="password"
              name="userPassword"
              placeholder="Enter Password"
              onChange={this.onChange}
            />
            <br />
            <Button
              bsStyle="primary"
              bsSize="small"
              onClick={this.onClickLogin}
              className="loginButton"
            >
              Login
            </Button>{" "}
            <Button
              bsStyle="primary"
              bsSize="small"
              onClick={this.onClickSignup}
              className="signupButton"
            >
              Sign up
            </Button>
            <div><h5>{errorMessage}</h5></div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginStatus: state.loginReducer
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ logIn }, dispatch);
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
