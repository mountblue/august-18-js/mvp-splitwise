import React, { Component } from 'react';
import { FormControl, Button } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createUser } from '../action/Action';
import '../styles/SignUp.css';

class Signup extends Component {
  constructor() {
    super();
    this.state = {
      userName: '',
      userEmail: '',
      userPassword: '',
    };
    this.addUser = this.addUser.bind(this);
    this.onChange = this.onChange.bind(this);
    this.cancel = this.cancel.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  addUser(e) {
    e.preventDefault();
    this.props.createUser(this.state);
    setTimeout(function(){
     if(!this.props.loginError.error)
     this.props.history.push("/")
    }.bind(this),100)
  }

  cancel(e) {
    e.preventDefault();
    this.props.history.push("/")
  }
  render() {
    return (
      <div className="signup">
      <div className="header">
          <img src="http://plates.splitwise.com/splitwise-logo-bordered.png" />
          <h1>Split expenses with friends.</h1>
        </div>
        <form>
          <h6>Name:</h6>
          <FormControl id="text" type="text" name="userName" placeholder="Enter Name" onChange={this.onChange} />
          <br />
          <h6>Email:</h6>
          <FormControl id="email" type="email" name="userEmail" placeholder="Enter Gmail" onChange={this.onChange} />
          <br />
          <h6>Password:</h6>
          <FormControl id="password" type="password" name="userPassword" placeholder="Enter Password" onChange={this.onChange} />
          <br />
          <Button bsStyle="primary" bsSize="small" onClick={this.cancel}>Cancel</Button>
          {' '}
          <Button bsStyle="primary"  bsSize="small" onClick={this.addUser}>Save</Button>
        </form>
      </div>
    );
  }
}

const mapStateToProps=(state)=>{
  return({loginError:state.signUpReducer})
}
const mapDispatchToProps=(dispatch)=> {
  return bindActionCreators({ createUser }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
