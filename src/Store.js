import { createStore, applyMiddleware, compose } from 'redux';
import  thunk  from 'redux-thunk';
import { reduxFirestore, getFirestore } from 'redux-firestore';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import  firebase from './config';
import allReducer from './reducer/CombineReducer';

const store = createStore(allReducer, compose(
    applyMiddleware(thunk.withExtraArgument({getFirebase,getFirestore})),
    reduxFirestore(firebase),
    reactReduxFirebase(firebase)
    ));

export default store;
