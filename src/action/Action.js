export const createUser = (userDetail) =>{
    return(dispatch,getState,{getFirebase,getFirestore})=>{
      const firestore=getFirestore();
      const firebase=getFirebase();
    firebase.auth().createUserWithEmailAndPassword(userDetail.userEmail, userDetail.userPassword)
    .then(()=>{
       return firestore.collection('users').add({
            userName:userDetail.userName,
            userEmail:userDetail.userEmail
        })
    }).then(()=>{
        dispatch({type:'CREATE_USER',payload:false})
        dispatch({type:'LOGIN_SUCCESS',payload:userDetail.userEmail})
    })
    .catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode === 'auth/weak-password') {
          window.alert('The password is too weak.');
        } else {
          window.alert(errorMessage);
        }
        dispatch({type:'CREATE_USER_ERROR',payload:true})
        dispatch({type:'LOGIN_FAILED',payload:errorMessage})
      });
    }
}
export const logIn = (credentials)=>{
  return(dispatch,getState,{getFirebase})=>{
    const firebase=getFirebase();
    console.log(credentials)
    firebase.auth().signInWithEmailAndPassword(credentials.userEmail, credentials.userPassword)
    .then(()=>{
      dispatch({type:'LOGIN_SUCCESS'})
    })
      .catch(function(error) {
      dispatch({type:'LOGIN_FAILED',payload:error.code})
    });
  }
}