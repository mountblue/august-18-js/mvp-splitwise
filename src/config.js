import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
const config = {
    apiKey: "AIzaSyBBV3VxFNEtKELWrzsuzaIBlvJ0UwDaq2I",
    authDomain: "mvp-splitwise.firebaseapp.com",
    databaseURL: "https://mvp-splitwise.firebaseio.com",
    projectId: "mvp-splitwise",
    storageBucket: "mvp-splitwise.appspot.com",
    messagingSenderId: "562576247950"
  };
  firebase.initializeApp(config);
  firebase.firestore().settings({timestampsInSnapshots:true});
  export  default firebase;