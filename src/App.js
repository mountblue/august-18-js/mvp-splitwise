import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './Store';
import Splitewise from './component/Splitwise';

class App extends Component {
  render() {
    console.log(store)
    return (
      <Provider store={store}>
        <div className="App">
          <Splitewise />
        </div>
      </Provider>
    );
  }
}

export default App;
