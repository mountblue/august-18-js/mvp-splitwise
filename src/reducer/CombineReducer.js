import { combineReducers } from 'redux';
import signUpReducer from './signUpReducer';
import loginReducer from './loginReducer'; 
import { firestoreReducer } from 'redux-firestore';
import {firebaseReducer} from 'react-redux-firebase';

const allReducer=combineReducers({signUpReducer,firestoreReducer,firebaseReducer,loginReducer})
console.log(firebaseReducer,"a")
export default allReducer