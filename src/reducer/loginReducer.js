const initialState={loginStatus:false,user:null,authError:null}
const loginReducer=(state=initialState,action)=>{
    switch(action.type)
    {
        case 'LOGIN_SUCCESS':
        state={...state,
        loginStatus:true,
        user:action.payload,
        authError:null
        }
        break;
        case 'LOGIN_FAILED':
        state={...state,
        loginStatus:false,
        user:null,
        authError:action.payload
        }
        break;
        default:
        return state;
    }
    return state
}
export default loginReducer