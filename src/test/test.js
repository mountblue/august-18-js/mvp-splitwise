const expect = require('assert');
const data = require('../data.json');
const compute = require('../compute');


  inputAddBill = [
    {  id: 1, description: 'food', paidby: 'abc', friends: ['def', 'ghi'], amount: 300 },
    {  id: 2, description: 'food', paidby: 'def', friends: ['abc', 'ghi'], amount: 900 }
  ]
  inputSettle =[
    { id:1, paidby: 'ghi', paidto: 'abc', amount:100 },
    { id:2, paidby: 'abc', paidto: 'def', amount:200 },
  ]

describe('Splitwise', () => {
  describe('Add bill', () => {
    it('Should be defined', () => {
      expect(typeof(compute.newbill), 'function');
    });
    it('When user pays the bill', () => {
      expect(compute.newbill(inputAddBill[0], data) === data)
    });
    it('When friend pays the bill', () => {
      expect(compute.newbill(inputAddBill[1],data) === data)
    });
  });
  describe('Settle bill', () => {
    it('Should be defined', () => {
      expect(typeof(compute.newsettle), 'function');
    });
    it('When a friend settles the bill', () => {
      expect(compute.newsettle(inputSettle[0], data) === data)
    });
    it('When user settles the bill', () => {
      expect(compute.newsettle(inputSettle[1], data) === data)
    });
  });
});
