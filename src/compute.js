const fs = require('fs');
function newbill(bill, data) {
  data[0].bills.push(bill);
  const n = bill.friends.length;
  const split = bill.amount / (n + 1);
  if (bill.friends.includes(data[0].name)) {
    data[0].friends.forEach((frnd) => {
      if (frnd.name === bill.paidby) {
        frnd.owes -= split;
      }
    })
  }
  else {
    data[0].friends.forEach((frnd) => {
      if (bill.friends.includes(frnd.name)) {
        frnd.owes += split;
      }
      return frnd;
    });
  }
  fs.writeFileSync('./data.json', JSON.stringify(data))
  return data
}

function newsettle(settle, data) {
  data[0].settle.push(settle)
  data[0].friends.forEach((frnd) => {
    if(frnd.name === settle.paidby && data[0].name === settle.paidto) {
      frnd.owes -= settle.amount;
    }
    if(frnd.name === settle.paidto && data[0].name === settle.paidby) {
      frnd.owes += settle.amount;
    }
  });
  fs.writeFileSync('./data.json', JSON.stringify(data))
  return data
}

module.exports = {
  newbill, newsettle
}